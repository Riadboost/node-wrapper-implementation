import Context from "./App/Context";
import Message from "./App/Message";
declare const _default: {
    AppInterface: any;
    Context: typeof Context;
    Message: typeof Message;
};
export default _default;
