"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Context {
    constructor(routingKey, consumerTag, isRedelivery) {
        this._routingKey = routingKey;
        this._consumerTag = consumerTag;
        this._isRedelivery = isRedelivery;
    }
    get routingKey() {
        return this._routingKey;
    }
    get consumerTag() {
        return this._consumerTag;
    }
    get isRedelivery() {
        return this._isRedelivery;
    }
}
exports.default = Context;
