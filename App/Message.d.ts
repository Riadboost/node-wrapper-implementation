export default class Message {
    private readonly _destination;
    private readonly _content;
    private readonly _transporter;
    constructor(destination: string, content: object, transporter: string);
    get destination(): string;
    get content(): string;
    get transporter(): string;
}
