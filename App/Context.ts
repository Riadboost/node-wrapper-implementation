export default class Context{
        private readonly _routingKey: string;
        private readonly _consumerTag: string
        private readonly _isRedelivery: boolean;

        constructor(routingKey: string, consumerTag: string, isRedelivery: boolean) {
            this._routingKey = routingKey;
            this._consumerTag = consumerTag;
            this._isRedelivery = isRedelivery;
        }


        get routingKey(): string {
            return this._routingKey;
        }

        get consumerTag(): string {
            return this._consumerTag;
        }

        get isRedelivery(): boolean {
            return this._isRedelivery;
        }
}