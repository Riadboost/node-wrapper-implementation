export default class Context {
    private readonly _routingKey;
    private readonly _consumerTag;
    private readonly _isRedelivery;
    constructor(routingKey: string, consumerTag: string, isRedelivery: boolean);
    get routingKey(): string;
    get consumerTag(): string;
    get isRedelivery(): boolean;
}
